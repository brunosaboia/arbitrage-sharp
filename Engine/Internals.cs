﻿using Jayrock.Json;
using KrakenClientConsole;
using Nextmethod.Cex;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public static class Internals
    {
        private const string kraken_dot_com = "kraken.com";
        private const string cex_dot_io = "cex.io";

        private static KrakenClient.KrakenClient _krakenClient = new KrakenClient.KrakenClient();
        private static Broker _broker = new Broker(_krakenClient);

        private static CexApi _cexClient = new CexApi();

        private static Dictionary<string, Dictionary<string, decimal>> _balancesByExchange = new Dictionary<string, Dictionary<string, decimal>>();
        private static Dictionary<string, Dictionary<string, decimal>> _pricesByExchange = new Dictionary<string, Dictionary<string, decimal>>();

        static Internals()
        {

        }

        public static void Run()
        {
            //for each exchange, get what we hold in all currencies...
            GetAllBalances();

            //ok, get all prices from all exchanges...
            GetAllPrices();
        }

        private static void GetAllBalances()
        {
            #region kraken.com         
            JsonObject balance = _krakenClient.GetBalance();

            if (balance != null)
            {
                if (!_balancesByExchange.ContainsKey(kraken_dot_com))
                {
                    _balancesByExchange.Add(kraken_dot_com, new Dictionary<string, decimal>());
                }

                foreach (string name in ((Jayrock.Json.JsonObject)balance["result"]).Names)
                {
                    string name2 = null;
                    if(name == "XETH")
                    {
                        name2 = "ETH";
                    }
                    else if (name == "XBTC")
                    {
                        name2 = "BTC";
                    }
                    else if (name == "ZUSD")
                    {
                        name2 = "USD";
                    }

                    if (!string.IsNullOrEmpty(name2))
                    {
                        if (!_balancesByExchange[kraken_dot_com].ContainsKey(name2))
                        {
                            _balancesByExchange[kraken_dot_com].Add(name2, 0);
                        }

                        _balancesByExchange[kraken_dot_com][name2] = decimal.Parse(((JsonObject)balance["result"])[name].ToString());
                    }
                }
            }
            #endregion

            #region cex.io
            Balance accountBalance = _cexClient.AccountBalance().Result;

            if (accountBalance != null)
            {
                if (!_balancesByExchange.ContainsKey(cex_dot_io))
                {
                    _balancesByExchange.Add(cex_dot_io, new Dictionary<string, decimal>());
                }

                if (!_balancesByExchange[cex_dot_io].ContainsKey(accountBalance.ETH.ToString()))
                {
                    _balancesByExchange[cex_dot_io].Add(accountBalance.ETH.ToString(), 0);
                }
                _balancesByExchange[cex_dot_io][accountBalance.ETH.ToString()] = accountBalance.ETH.Available;

                if (!_balancesByExchange[cex_dot_io].ContainsKey(accountBalance.BTC.ToString()))
                {
                    _balancesByExchange[cex_dot_io].Add(accountBalance.BTC.ToString(), 0);
                }
                _balancesByExchange[cex_dot_io][accountBalance.BTC.ToString()] = accountBalance.BTC.Available;

                if (!_balancesByExchange[cex_dot_io].ContainsKey(accountBalance.USD.ToString()))
                {
                    _balancesByExchange[cex_dot_io].Add(accountBalance.USD.ToString(), 0);
                }
                _balancesByExchange[cex_dot_io][accountBalance.USD.ToString()] = accountBalance.USD.Available;
            }
            #endregion
        }

        private static void GetAllPrices()
        {
            List<string> @pairs = new List<string>();

            @pairs.AddRange(ConfigurationManager.AppSettings["DesiredCryptoCurrencySymbols"].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

            #region kraken.com         
            JsonObject tickers = _krakenClient.GetTicker(@pairs);

            if (tickers != null)
            {
                if (!_pricesByExchange.ContainsKey(kraken_dot_com))
                {
                    _pricesByExchange.Add(kraken_dot_com, new Dictionary<string, decimal>());
                }

                foreach (string name in ((Jayrock.Json.JsonObject)tickers["result"]).Names)
                {
                    if (!_pricesByExchange[kraken_dot_com].ContainsKey(name))
                    {
                        _pricesByExchange[kraken_dot_com].Add(name, 0);
                    }

                    _pricesByExchange[kraken_dot_com][name] = decimal.Parse(((JsonArray)((JsonObject)((JsonObject)tickers["result"])[name])["a"])[0].ToString());
                }
            }
            #endregion
        }
    }
}
